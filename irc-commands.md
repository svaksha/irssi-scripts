- [IRC Commands](#irc-commands)
- [Resources](#resources)

# IRC Commands
Handy commands list from, http://xok.xorealms.com/manuals/xok-irc.pdf

- register nick : /msg NickServ REGISTER password email
- ID myself: /msg NickServ IDENTIFY password
- ID others: /msg NickServ IDENTIFY username password
- temp Op: /op username
- perm Op: /cs FLAGS #xokingdoms username AOP
- temp Voice: /mode #xokingdoms +v username
- auto Voice: /cs FLAGS #xokingdoms username +VA
- ban: /mode #xokingdoms +b [nick!user@host]
- unban: /mode #xokingdoms -b [nick!user@host]
- ban immunity: /mode #xokingdoms +e [nick!user@host]
- view ban list: /mode #xokingdoms +b
- SOP: /cs FLAGS #xokingdoms username SOP
- moderate channel: /mode #xokingdoms +m
- set topic: /cs topic #xokingdoms topic-message
- lock topic: /cs set TOPICLOCK
- register IRC channel: /cs REGISTER #xokingdoms
- cycle: /hop
- Add Nick to Group
1. login as original account
2. /ns identify newnick originalpassword
- or -
1. logout of any accounts /ns logout
2. /nick newnick
3. /ns identify originalnick originalpassword
4. /ns group originalpassword


# Resources
- [Useful IRC commands](http://wiki.mibbit.com/index.php/IRC_services)
- http://www.antionline.com/showthread.php?136933.html

